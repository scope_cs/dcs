# Welcome

There is DCS assignment 

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.

## Database Design

Here is Database Design

##User Schema

field |	type |	KEY |	Description
------|------| -----|-------
id|	  int         |	PK |Unique Idenifier of user
firstName|varchar(20) |	| The first Name of the user
lastName| varchar(20) |	|	The last Name of the user
email	| varchar(50) |	|	The email address of the user
dateOfBirth|datetime  |	|	The date of birth of the user
password | varchar(50)|	|	The user password in Hash
token |	varchar(20)   |	|	The random generated number for user access (with Salt)
balance |	float |	|	The account balance of the user
enabled |	int   |	|	User account enablement
dateOfCreated |	datetime| |		The creation date of the user account

##Item schema

field |	type |	KEY |	Description
------|------| -----|-------
id|	int|	PK|	The unique identifier of an item
name|	varchar(20)||		The name of an item
description|	varchar(100)||		The description of an item
photo|	varchar(100)||		The serialized HEX of a file
basePrice|	float||		The starting price of an item
bidPrice|	float||		The fixed price per bid for an item
ownerId|	int|	FK|	The identifier of the seller
currentPrice|	float	||	The highest bid amount
currentBidder|	varchar(50)|FK|	The highest bid user indenifier
dateOfCreated|	datetime||		The date time of the item creation
status|	varchar(20)|	|	The status of an item (In process, cancel, sold)
enabled|	int|	|	items enablement for soft deletion


Have fun!