/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demogui;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JTextArea;

/**
 *
 * @author patrick
 */
public class MultiServer {

    public static void main(String args[]) {
        /*
        ClientInfo clientInfo = new ClientInfo () ; 
        Broadcast broadcast = new Broadcast() ; 
        try {
            ServerSocket serverSocket = new ServerSocket(8080) ;
            System.out.println("Server Start Listening") ; 
            while (true) {
                Socket socket = serverSocket.accept() ; 
                broadcast.push(socket);
                new MultiServerThread(socket,clientInfo, broadcast).start();
            }
        } catch (Exception e) {
            System.out.println("Error : " + e.toString());
        }*/
        
    }
    
    private static volatile ServerSocket serverSocket;

    public void startServer (JTextArea serverConsole) {

               
        ClientInfo clientInfo = new ClientInfo () ; 
        Broadcast broadcast = new Broadcast() ; 
        try {
            serverSocket = new ServerSocket(8080) ;
            //System.out.println("Server Start Listening") ; 
            serverConsole.append("Server Start Listening \n");
            
            while (true) {
                Socket socket = serverSocket.accept() ; 
                broadcast.push(socket);
                new MultiServerThread(socket,clientInfo, broadcast,serverConsole).start();
            }
        } catch (Exception e) {
            System.out.println("Error : " + e.toString());
        } 
    }
    
    
    public void stopServer() {
        try{ 
            serverSocket.close();
        }catch (Exception e){
            System.out.println("Stop Server : " + e.toString());
        }
    }
    
    

}
