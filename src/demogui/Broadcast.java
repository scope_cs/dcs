/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demogui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Patrick
 */
public class Broadcast {
    private ArrayList<Socket> socketList = new ArrayList<Socket>() {} ; 
    
    public void push(Socket socket){
        socketList.add(socket);
    }
    
    public void send(String msg) throws IOException{
 
        for (Socket socket : this.socketList){

            PrintWriter out = new PrintWriter(socket.getOutputStream(), true) ; 
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream())) ; 
            
            out.println(msg) ; 
        }
    }
}
