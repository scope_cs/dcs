/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demogui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author patrick
 */
class MultiServerThread extends Thread {

    int portNumber = 8080;
    static final String welcomeMsg = "Hello successful link to server";
    ServerSocket serverSocket;
    Socket clientSocket;
    int i = 0;

    //==========init ClientInfo==============
    ClientInfo clientInfo = null;
    int clientId = 0;

    //==========Broadcast object ============
    Broadcast broadcast = null;

    //=========Server Console================
    JTextArea serverConsole = null;

    public MultiServerThread(Socket clientSocket, ClientInfo clientInfo, Broadcast broadcast, JTextArea serverConsole) {
        //this.serverSocket = serverSocket ; 

        //ServerSocket serverSocket = new ServerSocket(8080) ;
        this.clientSocket = clientSocket;
        this.clientInfo = clientInfo;
        this.broadcast = broadcast;
        this.serverConsole = serverConsole;
    }

    @Override
    public void run() {
        try {

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String userInput;
            synchronized (this.clientInfo) {
                if (this.clientId <= 0) {
                    this.clientId = this.clientInfo.getId();
                }
                out.println("Welcome you are client " + clientId);
                this.serverConsole.append("Client " + clientId + " has been connected \n");

            }

            while ((userInput = in.readLine()) != null) {
                //out.println("Output message to client "+ clientId + ":"+userInput);
                //out.println("Client " + clientId +":" + userInput);

                System.out.println("Output message to client " + clientId + ":" + userInput);

                
                synchronized (this.broadcast) {
                    this.broadcast.send("Client " + clientId + ":" + userInput);
                    this.serverConsole.append("Output message to client " + clientId + ":" + userInput + "\n");
                    
                }

                //System.out.println("Output message to client " + clientId + ":" + userInput);
            }

            out.close();
            in.close();

        } catch (Exception e) {
            System.out.println("Error in thread: " + e.toString());

        }

    }

}
