/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demogui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.lang.Runnable;
import java.lang.Thread;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kwannokng
 */
public class Client {

    public static void main(String args[]) {

        String hostName = "127.0.0.1";
        int portNumber = 8080;

        try {
            Socket socket = new Socket(hostName, portNumber);

            final PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            //ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));

            final BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

            String fromServer;
            //String[] names = {"1","2","3","4","5","6","7"} ; 

            new Thread(new Runnable() {
                public void run() {
                    String fromUser = "";
                    try {
                        while ((fromUser = stdIn.readLine()) != null) {

                            if (fromUser != null) {
                                //System.out.println("Client send message : " + fromUser);
                                //System.out.println(fromUser);
                                out.println(fromUser);
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }).start();


            new Thread(new Runnable() {
                public void run() {
                    String fromServer;
                    try {
                        while ((fromServer = in.readLine()) != null) {

                            //System.out.println("From Server : " + fromServer);
                            System.out.println(fromServer);
                        }
                    } catch (Exception e) {
                        System.out.println("Input method :" + e.toString());
                    }
                }
            }).start();
            /*
             while ((fromServer = in.readLine()) != null) {
             //out.writeObject(names);

             System.out.println("From Server : " + fromServer);

             String fromUser = stdIn.readLine();

             if (fromUser != null) {
             System.out.println("Client send message : " + fromUser);
             out.println(fromUser);
             }
             }*/

        } catch (Exception e) {
            System.out.println("Error : " + e.toString());
        }
    }
}
